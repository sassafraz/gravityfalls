#!/usr/bin/env python3
import sys, itertools, enchant
d = enchant.Dict("en_US")

if(len(sys.argv) != 2):
    sys.stderr.write("{0} <cipher>\n".format(sys.argv[0]))
    sys.exit(1)

txt = sys.argv[1].lower().replace(" ", "")
txtspc = [i for i, char in enumerate(sys.argv[1].lower()) if char == " "]
keystream = ""
key = ""

for k in range(20):
    for m in itertools.product("abcdefghijklmnopqrstuvwxyz", repeat=k+1):
        res = ""
        key = str("".join(m))
        keystream = ""

        print(key, end = "\r")
        #print(key)
        for i in range(len(txt)):
            keystream += key[i%len(key)]

        def deciph(x, y):
            l0 = ord(x)-96
            l1 = ord(y)-96
            asdf = (l0-l1)%26
            return chr(asdf+1+96)
        def decode(x):
            cipher = ""
            for i in range(len(x)):
                cipher += deciph(x[i], keystream[i])
            return cipher
        res = decode(txt)
        for i in txtspc:
            res = res[:i] + " " + res[i:]
        a = True
        for i in res.split(" "):
            if not(d.check(i)):
                a = False
        #if(d.check(res.split(" ")[0])):
        if(a):
            print("{0}: {1}".format(key, res))
            #sys.exit(0)
