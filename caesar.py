#!/usr/bin/env python3
import sys

if(len(sys.argv) != 2):
    sys.stderr.write("{0} <text>>\n".format(sys.argv[0]))
    sys.exit(1)

dec = sys.argv[1].lower()
ans = ""
alpha = "abcdefghijklmnopqrstuvwxyz"

# shift throughout the whole alphabet
for i in range(26):
    ans += "+" + str((-3-i)%26) + ": "
    for word in dec.split(" "):
        for letter in word:
            ans += alpha[(ord(letter)-96-4-i)%26]
        ans += " "
    ans += "\n"
print(ans)
