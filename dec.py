#!/usr/bin/env python3
import sys, re, enchant

d = enchant.Dict("en_US")
f = open("gf_results.txt", "r+")

if(len(sys.argv) < 2):
    sys.stderr.write("{0} <text> <key (vigenere cipher)>\n".format(sys.argv[0]))
    sys.exit(1)

dec = sys.argv[1]
alpha = "abcdefghijklmnopqrstuvwxyz"
res = ""
ans = ""
ans1 = ""
cipher = ""

# check if answer is valid
def isvalid(x):
    v = False
    for i in x.split(" "):
        if(d.check(i)):
            v = True
    return v

if(len(sys.argv) == 3):
    # vigenere cipher
    dec = sys.argv[1].lower().replace(" ", "")
    key = sys.argv[2].lower().replace(" ", "")
    txtspc = [i for i, char in enumerate(sys.argv[1].lower()) if char == " "]
    keystream = ""

    for i in range(len(dec)):
        keystream += key[i%len(key)]

    def deciph(x, y):
        l0 = ord(x)-96
        l1 = ord(y)-96
        res = (l0-l1)%26
        return chr(res+1+96)
    def decode(x):
        cip = ""
        for i in range(len(x)):
            cip += deciph(x[i], keystream[i])
        return cip
    res = decode(dec)
    for i in txtspc:
        res = res[:i] + " " + res[i:]
    cipher = "VIGENERE"
    print(res)
    wr = "{0} - key: {1} | {2}: {3}".format(cipher, key, sys.argv[1].lower(), res)
else:
    for word in dec.split(" "):
        # A1Z26 cipher
        if(dec[0].isdigit()):
            cipher = "A1Z26"
            for k in word.split("-"):
                res += alpha[int(k)-1]
        elif(dec[0].isalpha()):
            for k in word.lower():
                # caesar
                ans += alpha[ord(k)-96-4]
                # atbash
                ans1 += alpha[::-1][ord(k)-97]
        res += " "
        ans += " "
        ans1 += " "
    if(dec[0].isdigit()):
        cipher = "A1Z16"
    elif(d.check(ans.split(" ")[0]) or d.check(ans.split(" ")[1])):
        res = ans
        cipher = "CAESAR"
    else:
        res = ans1
        cipher = "ATBASH"
    print("{0}: {1}".format(cipher, res))
    wr = "{0} - {1}: {2}".format(cipher, sys.argv[1].lower(), res)

# write answers to file
if not any(wr == x.rstrip('\r\n') for x in f):
    print(wr, file=f)
f.close()
